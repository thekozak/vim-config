set nocompatible              " be iMproved


" ==========================================================
" === Vundle settings

filetype off                  " required!

set rtp+=~/.vim/bundle/vundle/
call vundle#rc()

" let Vundle manage Vundle
" required! 
Plugin 'gmarik/vundle'

" My bundles here:
" original repos on GitHub

" Used
Plugin 'scrooloose/nerdtree.git'
Plugin 'scrooloose/nerdcommenter.git'
Plugin 'mhinz/vim-startify'
Plugin 'Valloric/YouCompleteMe'
Plugin 'majutsushi/tagbar'

" To configure
Plugin 'bling/vim-airline'
Plugin 'edkolev/tmuxline.vim'

" to check
Plugin 'tpope/vim-fugitive'
Plugin 'Lokaltog/vim-easymotion'
Plugin 'rstacruz/sparkup', {'rtp': 'vim/'}
Plugin 'tpope/vim-surround'
Plugin 'tomtom/tlib_vim.git'
Plugin 'MarcWeber/vim-addon-mw-utils.git'
Plugin 'garbas/vim-snipmate.git'
Plugin 'honza/vim-snippets.git'
"Plugin 'robhudson/snipmate_for_django'
Plugin 'kien/ctrlp.vim'
Plugin 'vim-scripts/cscope.vim'
Plugin 'airblade/vim-gitgutter'
Plugin 'dantler/vim-alternate'
Plugin 'vim-scripts/YankRing.vim'
Plugin 'sjl/gundo.vim'


" *** NEW ***
" Ag plugin
Plugin 'derekwyatt/ag.vim'
Plugin 'derekwyatt/vim-sbt'
Plugin 'elzr/vim-json'
Plugin 'bling/vim-bufferline'
"Plugin 'sjl/gundo.vim'
"nnoremap <F5> :GundoToggle<CR>

Plugin 'mbbill/undotree'
nnoremap <F5> :UndotreeToggle<cr>


" programming languages plugins
Plugin 'derekwyatt/vim-scala'
Plugin 'kchmck/vim-coffee-script'
"Plugin 'tpope/vim-rails'
"Plugin 'lukerandall/haskellmode-vim'
"Plugin 'klen/python-mode'


" AngularJS and Javascript plugins
" burnettk/vim-angular
" pangloss/vim-javascript
" othree/javascript-libraries-syntax.vim
" matthewsimo/angular-vim-snippets
" claco/jasmine.vim
" scrooloose/syntastic


" To check
"Plugin 'ivanov/vim-ipython'
"Plugin 'xolox/vim-pyref'
"Plugin 'xolox/vim-misc'

" !!! GIVES ERROR !!!
"Plugin 'vim-scripts/vtimer.vim'
" Plugin 'vim-scripts/Conque-Shell'
" Should be installed manually
" http://code.google.com/p/conque/
" !!! GIVES ERROR !!!

" see :h vundle for more details or wiki for FAQ
" NOTE: comments after Plugin commands are not allowed.


" ==========================================================
" === General settings

" Also required by Vundle
syntax on
filetype plugin indent on     

" let mapleader=","

" http://nvie.com/posts/how-i-boosted-my-vim/
set nowrap        " don't wrap lines
set tabstop=4     " a tab is four spaces
set backspace=indent,eol,start
                  " allow backspacing over everything in insert mode
set autoindent    " always set autoindenting on
set copyindent    " copy the previous indentation on autoindenting
set number        " always show line numbers
set shiftwidth=4  " number of spaces to use for autoindenting
set shiftround    " use multiple of shiftwidth when indenting with '<' and '>'
set showmatch     " set show matching parenthesis
set ignorecase    " ignore case when searching
set smartcase     " ignore case if search pattern is all lowercase,
		  " case-sensitive otherwise
set smarttab      " insert tabs on the start of a line according to
		  " shiftwidth, not tabstop
set hlsearch      " highlight search terms
set incsearch     " show search matches as you type
set history=1000	" remember more commands and search history
set wildignore=*.swp,*.bak,*.pyc,*.class,*.o

" save often instead of keeping backups and swap files
set nobackup
set noswapfile

" Pasting large amounts of text into Vim
set pastetoggle=<F9>

" Easy window navigation
map <C-h> <C-w>h
map <C-j> <C-w>j
map <C-k> <C-w>k
map <C-l> <C-w>l

" Finally, a trick by Steve Losh for when you forgot to sudo before 
" editing a file that requires root privileges (typically /etc/hosts).
" This lets you use w!! to do that after you opened the file already:
cmap w!! w !sudo tee % >/dev/null



" ==========================================================
" === Persistent undo
set undofile                " Save undo's after file closes
set undodir=$HOME/.vim/undo " where to save undo histories
set undolevels=1000         " How many undos
set undoreload=10000        " number of lines to save for undo



" http://linuxlefty.com/tools/favorite-vim-plugins-vi-gvim.html


" ==========================================================
" === NerdTree
nmap <F2> :NERDTreeToggle<CR>


" ==========================================================
" === Tagbar
nmap <F3> :TagbarToggle<CR>


" ==========================================================
" === ctrlp
let g:ctrlp_map = '<c-p>'
let g:ctrlp_cmd = 'CtrlP'


" ==========================================================
" === cscope
let g:cscope_cmd = '/usr/bin/cscope'


" ==========================================================
" === haskellmode-vim
let g:haddock_browser="/usr/bin/firefox"
" let g:haddock_docdir="/usr/local/share/doc/ghc/html/"


" ==========================================================
" === vim-snipmate
let g:snips_author='Lukasz Kozlowski'
let g:author='Lukasz Kozlowski'
let g:snips_email='lukasz.t.kozlowski@gmail.com'
let g:email='lukasz.t.kozlowski@gmail.com'
let g:snips_github='https://github.com/the-kozak'
let g:github='https://github.com/the-kozak'


" === snipmate_for_django
" TODO przetestowac i dopracowac
function! StartDjango(...)
	:set ft=python.django
	:set ft=htmldjango.html
endfunction

" nmap <F9> :call StartDjango()


" ==========================================================
" === python-mode
let g:pymode_run = 1
let g:pymode_run_key = '<leader>r'

let g:pymode_rope = 1
let g:pymode_lint_write = 0


" ==========================================================
" === Omincomplete for Python + Django
" === from: http://blog.fluther.com/django-vim/
" === see also: https://code.djangoproject.com/wiki/UsingVimWithDjango
autocmd FileType python set omnifunc=pythoncomplete#Complete
autocmd FileType javascript set omnifunc=javascriptcomplete#CompleteJS
autocmd FileType html set omnifunc=htmlcomplete#CompleteTags
autocmd FileType css set omnifunc=csscomplete#CompleteCSS


" ==========================================================
" === vim-airline
set t_Co=256
set laststatus=2
let g:airline_theme='murmur'
let g:airline_detect_whitespace=0
let g:airline#extensions#tmuxline#enabled = 0

" === tmuxline
let g:tmuxline_powerline_separators = 0
let g:tmucline_theme='zenburn'

let g:tmuxline_preset = {
      \'a'    : '#S',
      \'c'    : ['#(whoami)', '#(uptime | cut -d " " -f 1,2,3)'],
      \'win'  : ['#I', '#W'],
      \'cwin' : ['#I', '#W', '#F'],
      \'y'    : ['%R', '%a', '%d %b'],
      \'z'    : '#H'}

let g:tmuxline_separators = {
    \ 'left' : '',
    \ 'left_alt': '>',
    \ 'right' : '',
    \ 'right_alt' : '<',
    \ 'space' : ' '}

" ==========================================================
" === vtimer
let g:vtimerPath = '/.vim/bundle/vtimer.vim/plugin/vtimer.vim'


