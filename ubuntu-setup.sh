#!/bin/bash

UBUNTU_PACKAGES="gnome-shell gnome-shell-extensions tmux git build-essential cmake python-dev python ipython 
silversearcher-ag colordiff colormake colorgcc gcc g++ firefox chromium-browser 
ruby terminator mc putty minicom geany geany-plugins doxygen meld graphviz
ipython-notebook ipython-qtconsole python-matplotlib pylint python-pip clang tree"


#sudo rm /bin/sh
#sudo ln -s /bin/bash /bin/sh
#sudo apt-get update && sudo apt-get upgrade
sudo apt-get install $UBUNTU_PACKAGES

# disable apport messages
# /etc/default/apport change enable=1 to enable=0


