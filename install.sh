#!/bin/bash


# How to install vim from sources:
#sudo apt-get install libncurses5-dev libgnome2-dev libgnomeui-dev \
#libgtk2.0-dev libatk1.0-dev libbonoboui2-dev \
#libcairo2-dev libx11-dev libxpm-dev libxt-dev python-dev ruby-dev mercurial

#./configure --with-features=huge --enable-multibyte --enable-rubyinterp \
#--enable-pythoninterp --with-python-config-dir=/usr/lib/python2.7/config \
#--enable-perlinterp --enable-luainterp --enable-gui=gtk2 --enable-cscope \
#--prefix=/usr

#make VIMRUNTIMEDIR=/usr/share/vim/vim74


# For YouCompleteMe additional packages are needed:
#sudo apt-get install build-essential cmake python-dev


OS=$(lsb_release -si)

echo "Discovered operating system: $OS"
if [ $OS = "Ubuntu" ]
then
	bash ./ubuntu-setup.sh
elif [ $OS = "Arch" ]
then
	bash ./arch-setup.sh
else
	echo "Unrecognized distro. Please install tmux manually"
fi

if [ $? -ne 0 ]; then
	echo "System configuration was not successful. Exiting."
	exit 1
fi

PROJECT_DIR=`pwd`
cd ~
HOME_DIR=`pwd`

BASHRC=~/.bashrc
VIM_DIR=~/.vim
VIM_UNDO_DIR=$VIM_DIR/undo
VIMRC_FILE=~/.vimrc
TMUXCONF_FILE=~/.tmux.conf
TMUXLINE_FILE=~/.tmuxline_snapshot.conf

echo " Installing Vundle... "
git clone https://github.com/gmarik/vundle.git $VIM_DIR/bundle/vundle
echo " Installing Vundle...              [DONE]"

echo " Copying .vimrc...  "
cd $HOME_DIR
if [ -e $VIMRC_FILE ]
then
	echo "Moving $VIMRC_FILE to $VIMRC_FILE.bak"
	mv $VIMRC_FILE $VIMRC_FILE.bak
fi
cp $PROJECT_DIR/dot_vimrc $VIMRC_FILE
echo " Copying .vimrc...                 [DONE]"

if [ ! -d $VIM_UNDO_DIR ]
then
	echo "Creating directory for vim's persistent undo"
	mkdir -p $VIM_UNDO_DIR
fi

#echo "==================================="
#echo " Now run vim ant execute command:  "
#echo " :BundleInstall                    "
#echo "==================================="

echo " Installing plugins..."
vim -E -c BundleInstall -c q -c q
echo " Installing plugins...             [DONE]"

echo " Installing YouComleteMe plugin... "
cd ~/.vim/bundle/YouCompleteMe
./install.sh --clang-completer
cd -
echo " Installing YouComleteMe plugin... [DONE]"

echo " Installing tmux... "

cd $PROJECT_DIR

if [ -e $TMUXCONF_FILE ]
then
	echo "Moving $TMUXCONF_FILE to $TMUXCONF_FILE.bak"
	mv $TMUXCONF_FILE $TMUXCONF_FILE.bak
fi
cp dot_tmux.conf $TMUXCONF_FILE

if [ -e $TMUXLINE_FILE ]
then
	echo "Moving $TMUXLINE_FILE to $TMUXLINE_FILE.bak"
	mv $TMUXLINE_FILE $TMUXLINE_FILE.bak
fi
cp dot_tmuxline_snapshot.conf $TMUXLINE_FILE

echo " Installing tmux...                [DONE]"


echo " Installing tmuxinator... "
sudo gem install tmuxinator
wget https://raw.githubusercontent.com/tmuxinator/tmuxinator/master/completion/tmuxinator.bash
mkdir -p ~/.bin
mv tmuxinator.bash ~/.bin/tmuxinator.bash
cp $BASHRC $BASHRC.bak
echo "export EDITOR='vim'" >> $BASHRC
echo "source ~/.bin/tmuxinator.bash" >> $BASHRC
echo " Installing tmuxinator...          [DONE]"

